from django.apps import AppConfig


class ExchangeRatesAppConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'exchange_rates_app'
