from django.shortcuts import render
from ninja import NinjaAPI, Schema
import requests
from datetime import datetime
from django.shortcuts import render


cache_exchange_rates = []
CACHE_SIZE = 10
TIME_LIMIT = 10 # разрешается 1 раз в 10 секунд

def add_exchange_rates(exchange_rates: any) -> None:
    global cache_exchange_rates
    cache_exchange_rates.append(exchange_rates)
    if len(cache_exchange_rates) > CACHE_SIZE:
        cache_exchange_rates = cache_exchange_rates[-CACHE_SIZE:]
        
class Error(Schema):
    message: str

class ExchangeRates(Schema):
    exchange_rates: list

exchange_rates = NinjaAPI()

@exchange_rates.get('/')
def index(request):
    return render(request, "index.html")

@exchange_rates.get('/get-current-usd', response={
    200: ExchangeRates,
    425: Error, # слишком рано
    429: Error, # слишком много запросов
    500: Error, # внутренняя ошибка сервера
    503: Error, # сервис недоступен
})
def get_current_usd(request):
    # Условия сервиса: 
    # Раз, не более 5 запросов в секунду, 
    # 120 запросов в минуту с одного IP и 
    # не более 10000 запросов в сутки, пожалуйста.
    BASE = "RUB"
    RATE = "USD"
    API_URL = "https://www.cbr-xml-daily.ru/latest.js"
    
    if cache_exchange_rates:
        now = datetime.now()
        last_request = cache_exchange_rates[-1].get('datetime')
        difference = now - last_request
        if difference.seconds < 10:
            return 425, {"message": "Too Early"}

    response = requests.get(API_URL)
    if not response.status_code == 200:
        return 503, {"message": "Service Unavailable"}
    
    try:
        response = response.json()
    except requests.exceptions.JSONDecodeError as e:
        print(e)
        return 500, {"message": "Internal Server Error"}
    else:
        if response.get("base") != BASE:
            return 500, {"message": "Internal Server Error"}
        exchange_rates = {}
        exchange_rates['datetime'] = datetime.now()
        exchange_rates['date-actual-info'] = response.get('date')
        exchange_rates['base'] = response.get('base')
        exchange_rates['rate'] = RATE
        exchange_rates['value'] = response.get('rates', {}).get(RATE)
        add_exchange_rates(exchange_rates)
        return {"exchange_rates": cache_exchange_rates}
